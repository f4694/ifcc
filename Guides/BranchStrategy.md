## Branch Strategy

1. Main branch is used only for deployment.
2. Only QA branch is allowed to be merged to main branch.
3. Each team has their own "develop" branch. Named after team number.
4. When "committing" write down a precise meaningful message.
5. "Git pull" often, before starting to work.
6. 